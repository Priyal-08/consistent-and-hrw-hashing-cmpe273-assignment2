import csv

def parse(file_name):
    with open(file_name) as csv_file:
        csv_data = csv.reader(csv_file, delimiter=',')
        line_number = 0
        for row in csv_data:
            if line_number == 0:
                line_number += 1
                continue
            else:
                data = {'Year': row[0], 'Cause Name': row[2],
                        'State': row[3], 'Data': ','.join(map(str, row))}
                line_number += 1
                yield data
