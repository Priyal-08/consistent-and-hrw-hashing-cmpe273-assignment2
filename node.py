import mmh3
import requests
import json


class Node:
    def __init__(self, url):
        self.url = url
        self.hash = mmh3.hash(self.url)
        self.data_store = {}

    def get_url(self):
        return self.url

    def get_hash(self):
        return self.hash

    def post_data(self, key, data):
        payload = {key: data}
        headers = {'content-type': 'application/json'}
        requests.post(self.url+'/api/v1/entries',
                      data=json.dumps(payload), headers=headers)

    def get_data(self):
        response = requests.get(self.url+'/api/v1/entries')
        print("\nGET {}".format(self.url))
        print(json.dumps(response.json(), indent=2))
