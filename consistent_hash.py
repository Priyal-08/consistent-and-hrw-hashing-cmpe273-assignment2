import mmh3
from collections import OrderedDict
import csv_paser
import node as server
import sys
import yaml

class ConsistentHashing:
    def __init__(self, file_name, servers):
        self.node_dict = OrderedDict()
        for server in servers:
            self.add_node(server)
        self.push_data(file_name)
        self.get_data()

    def push_data(self, file_name):
        num_records = 0
        for row in csv_paser.parse(file_name):
            num_records += 1
            key = self.generate_key(
                row['Year'], row['Cause Name'], row['State'])
            self.send_data_to_node(key, row['Data'])
        print('Uploaded all {} entries.'.format(num_records))

    def get_data(self):
        print('Verifying the data.')
        for node_key in self.node_dict.keys():
            self.node_dict[node_key].get_data()

    def generate_key(self, year, cause, state):
        return mmh3.hash('{}:{}:{}'.format(year, cause, state))

    def add_node(self, url):
        node = server.Node(url)
        self.node_dict[node.get_hash()] = node

    def send_data_to_node(self, key_hash, data):
        for node_key in sorted(self.node_dict.keys()):
            if node_key > key_hash:
                self.node_dict[node_key].post_data(key_hash, data)
                return
        node_key = min(self.node_dict.keys())
        self.node_dict[node_key].post_data(key_hash, data)
        return

if __name__ == '__main__':
    csv_file = sys.argv[1]
    servers = 'http://localhost:5000'  # default server
    with open("config.yaml", 'r') as stream:
        try:
            servers = yaml.load(stream)['servers']
        except yaml.YAMLError as exc:
            print(exc)
    ConsistentHashing(csv_file, servers)
