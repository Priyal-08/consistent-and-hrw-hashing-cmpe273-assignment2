from flask import Flask, Response, request
from flask_restful import reqparse, abort, Api, Resource
import json
import sys

app = Flask(__name__)
api = Api(app)
entries = []

# shows a list of all entries, and lets you POST to add new entry
class EntryList(Resource):
    def get(self):
        response = {
            "num_entries": len(entries),
            "entries": entries
        }
        return response

    def post(self):
        data = request.get_json()
        entries.append(data)
        return '', 201


api.add_resource(EntryList, '/api/v1/entries')

if __name__ == '__main__':
    port = sys.argv[1]
    app.run(port=port, debug=True)
