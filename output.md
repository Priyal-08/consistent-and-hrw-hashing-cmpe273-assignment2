Data distribution analysis:

# Consistent Hashing:-

LM-SJC-11013881:cmpe273-assignment2 mangyadav\$ python3 consistent_hash.py causes-of-death.csv

Uploaded all 10296 entries.

Verifying the data.

GET http://localhost:5000

{

"num_entries": 1179

}

GET http://localhost:5001

{

"num_entries": 539

}

GET http://localhost:5002

{

"num_entries": 3601

}

GET http://localhost:5003

{

"num_entries": 4977

}

# Rendezvous Hashing:-

LM-SJC-11013881:cmpe273-assignment2 mangyadav\$ python3 hrw_hash.py causes-of-death.csv

Uploaded all 10296 entries.

Verifying the data.

GET http://localhost:5000

{

"num_entries": 2577

}

GET http://localhost:5001

{

"num_entries": 2606

}

GET http://localhost:5002

{

"num_entries": 2552

}

GET http://localhost:5003

{

"num_entries": 2561

}
